#pragma once
#include "MainWindow.h"

namespace ProtNieruch1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text::RegularExpressions;

	/// <summary>
	/// Podsumowanie informacji o NieruchomoscWindow
	/// </summary>
	public ref class NieruchomoscWindow : public System::Windows::Forms::Form
	{
	public:
		NieruchomoscWindow(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~NieruchomoscWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  Nieruchomosc;
	protected:
	private: System::Windows::Forms::Label^  Adres;
	private: System::Windows::Forms::Label^  Powierzchnia;
	private: System::Windows::Forms::Label^  DataSprzedazy;
	private: System::Windows::Forms::Label^  CenaCalkowita;
	private: System::Windows::Forms::TextBox^  Adres_txt;
	private: System::Windows::Forms::TextBox^  Powierzchnia_txt;
	private: System::Windows::Forms::TextBox^  DataSprzedazy_txt;
	private: System::Windows::Forms::TextBox^  CenaCalkowita_txt;
	private: System::Windows::Forms::Button^  DodajNieruchomosc;
	private: System::Windows::Forms::ErrorProvider^  eProvider;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Metoda wymagana do obs�ugi projektanta � nie nale�y modyfikowa�
		/// jej zawarto�ci w edytorze kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->Nieruchomosc = (gcnew System::Windows::Forms::Label());
			this->Adres = (gcnew System::Windows::Forms::Label());
			this->Powierzchnia = (gcnew System::Windows::Forms::Label());
			this->DataSprzedazy = (gcnew System::Windows::Forms::Label());
			this->CenaCalkowita = (gcnew System::Windows::Forms::Label());
			this->Adres_txt = (gcnew System::Windows::Forms::TextBox());
			this->Powierzchnia_txt = (gcnew System::Windows::Forms::TextBox());
			this->DataSprzedazy_txt = (gcnew System::Windows::Forms::TextBox());
			this->CenaCalkowita_txt = (gcnew System::Windows::Forms::TextBox());
			this->DodajNieruchomosc = (gcnew System::Windows::Forms::Button());
			this->eProvider = (gcnew System::Windows::Forms::ErrorProvider(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->eProvider))->BeginInit();
			this->SuspendLayout();
			// 
			// Nieruchomosc
			// 
			this->Nieruchomosc->AutoSize = true;
			this->Nieruchomosc->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Nieruchomosc->Location = System::Drawing::Point(214, 9);
			this->Nieruchomosc->Name = L"Nieruchomosc";
			this->Nieruchomosc->Size = System::Drawing::Size(99, 15);
			this->Nieruchomosc->TabIndex = 0;
			this->Nieruchomosc->Text = L"Nieruchomosc";
			// 
			// Adres
			// 
			this->Adres->AutoSize = true;
			this->Adres->Location = System::Drawing::Point(12, 48);
			this->Adres->Name = L"Adres";
			this->Adres->Size = System::Drawing::Size(34, 13);
			this->Adres->TabIndex = 1;
			this->Adres->Text = L"Adres";
			// 
			// Powierzchnia
			// 
			this->Powierzchnia->AutoSize = true;
			this->Powierzchnia->Location = System::Drawing::Point(12, 72);
			this->Powierzchnia->Name = L"Powierzchnia";
			this->Powierzchnia->Size = System::Drawing::Size(70, 13);
			this->Powierzchnia->TabIndex = 2;
			this->Powierzchnia->Text = L"Powierzchnia";
			// 
			// DataSprzedazy
			// 
			this->DataSprzedazy->AutoSize = true;
			this->DataSprzedazy->Location = System::Drawing::Point(12, 98);
			this->DataSprzedazy->Name = L"DataSprzedazy";
			this->DataSprzedazy->Size = System::Drawing::Size(80, 13);
			this->DataSprzedazy->TabIndex = 3;
			this->DataSprzedazy->Text = L"Data sprzeda�y";
			// 
			// CenaCalkowita
			// 
			this->CenaCalkowita->AutoSize = true;
			this->CenaCalkowita->Location = System::Drawing::Point(12, 124);
			this->CenaCalkowita->Name = L"CenaCalkowita";
			this->CenaCalkowita->Size = System::Drawing::Size(82, 13);
			this->CenaCalkowita->TabIndex = 4;
			this->CenaCalkowita->Text = L"Cena ca�kowita";
			// 
			// Adres_txt
			// 
			this->Adres_txt->Location = System::Drawing::Point(134, 45);
			this->Adres_txt->Name = L"Adres_txt";
			this->Adres_txt->Size = System::Drawing::Size(413, 20);
			this->Adres_txt->TabIndex = 5;
			// 
			// Powierzchnia_txt
			// 
			this->Powierzchnia_txt->Location = System::Drawing::Point(134, 69);
			this->Powierzchnia_txt->Name = L"Powierzchnia_txt";
			this->Powierzchnia_txt->Size = System::Drawing::Size(413, 20);
			this->Powierzchnia_txt->TabIndex = 6;
			// 
			// DataSprzedazy_txt
			// 
			this->DataSprzedazy_txt->Location = System::Drawing::Point(134, 95);
			this->DataSprzedazy_txt->Name = L"DataSprzedazy_txt";
			this->DataSprzedazy_txt->Size = System::Drawing::Size(413, 20);
			this->DataSprzedazy_txt->TabIndex = 7;
			// 
			// CenaCalkowita_txt
			// 
			this->CenaCalkowita_txt->Location = System::Drawing::Point(134, 121);
			this->CenaCalkowita_txt->Name = L"CenaCalkowita_txt";
			this->CenaCalkowita_txt->Size = System::Drawing::Size(413, 20);
			this->CenaCalkowita_txt->TabIndex = 8;
			// 
			// DodajNieruchomosc
			// 
			this->DodajNieruchomosc->Location = System::Drawing::Point(226, 153);
			this->DodajNieruchomosc->Name = L"DodajNieruchomosc";
			this->DodajNieruchomosc->Size = System::Drawing::Size(75, 23);
			this->DodajNieruchomosc->TabIndex = 9;
			this->DodajNieruchomosc->Text = L"Dodaj";
			this->DodajNieruchomosc->UseVisualStyleBackColor = true;
			this->DodajNieruchomosc->Click += gcnew System::EventHandler(this, &NieruchomoscWindow::DodajNieruchomosc_Click);
			// 
			// eProvider
			// 
			this->eProvider->ContainerControl = this;
			// 
			// NieruchomoscWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(597, 188);
			this->Controls->Add(this->DodajNieruchomosc);
			this->Controls->Add(this->CenaCalkowita_txt);
			this->Controls->Add(this->DataSprzedazy_txt);
			this->Controls->Add(this->Powierzchnia_txt);
			this->Controls->Add(this->Adres_txt);
			this->Controls->Add(this->CenaCalkowita);
			this->Controls->Add(this->DataSprzedazy);
			this->Controls->Add(this->Powierzchnia);
			this->Controls->Add(this->Adres);
			this->Controls->Add(this->Nieruchomosc);
			this->Name = L"NieruchomoscWindow";
			this->Text = L"Nieruchomosc";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->eProvider))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

public: System::Void DodajNieruchomosc_Click(System::Object^  sender, System::EventArgs^  e) 
{
	String^ folder = "Nieruchomosc";
	array<String^>^ directory = Directory::GetFileSystemEntries(folder);
	int transaction = directory->Length + 1;

	if (directory->Length > 0) {
		int i = 0;
		int k = 1;
		while (directory->Length > i) {
			if (directory[i] == "Nieruchomosc\\" + k + ".csv") {
				if (directory->Length == k) {
					k = k + 1;
					createandSaveCSV("Nieruchomosc\\" + k + ".csv", k);
					break;
				}
				else {
					k = k + 1;
				}
			}
			else {
				createandSaveCSV("Nieruchomosc\\" + k + ".csv", k);
				break;
			}
			i = i + 1;
		}
	}
	else
	{
		createandSaveCSV("Nieruchomosc\\" + transaction + ".csv", transaction);
	}
}


//Saving CSV file for transactions if validations are correct

private: void createandSaveCSV(System::String^ filename, int Lpp)
{
	int validation = Validating();

	if (validation > 0) 
	{
		MessageBox::Show("W formularzu znajduj� si� b��dy");
	}
	else 
	{
		// file pointer 

		StreamWriter^ fout = gcnew StreamWriter(filename);
		String^ folder = "Nieruchomosc";
		array<String^>^ directory = Directory::GetFileSystemEntries(folder);

		String^ Lp = System::Convert::ToString(Lpp);


		fout->Write(Lp + ",");
		fout->Write(Adres_txt->Text + ",");
		fout->Write(CenaCalkowita_txt->Text + ",");
		fout->Write(DataSprzedazy_txt->Text + ",");
		fout->Write(Powierzchnia_txt->Text);
		fout->Close();

		NieruchomoscWindow::Close();

		MessageBox::Show("Dodano transakcj� poprawnie");
	}
}

private: int Validating() {

	int flag = 0;

	if (Adres_txt->Text == "") {
		eProvider->SetError(Adres_txt, "Pole nie mo�e by� puste");
		flag++;
	}
	else 
	{
		eProvider->SetError(Adres_txt, "");
	}

	if (Powierzchnia_txt->Text == "") {
		eProvider->SetError(Powierzchnia_txt, "Pole nie mo�e by� puste");
		flag++;
	}
	else 
	{
		eProvider->SetError(Powierzchnia_txt, "");
	}

	if (CenaCalkowita_txt->Text == "") {
		eProvider->SetError(CenaCalkowita_txt, "Pole nie mo�e by� puste");
		flag++;
	}
	else
	{
		eProvider->SetError(CenaCalkowita_txt, "");
	}

	if (DataSprzedazy_txt->Text == "") {
		eProvider->SetError(DataSprzedazy_txt, "Pole nie mo�e by� puste");
		flag++;
	}
	else
	{
		eProvider->SetError(DataSprzedazy_txt, "");
	}

	String^ pattern = "^[0-9]+(\.[0-9]{1,2})?$";
	Regex^ regex = gcnew Regex(pattern);

	if (CenaCalkowita_txt->Text != "") {
		bool match = regex->IsMatch(CenaCalkowita_txt->Text);
		if (!match) {
			eProvider->SetError(CenaCalkowita_txt, "Nale�y poda� do 2 cyfr po kropce");
			flag++;
		}
	}

	return flag;
}



};
}
