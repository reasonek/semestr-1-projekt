#include "MainWindow.h"
#include "NieruchomoscWindow.h"
#include <fstream>
#include <chartdir.h>

using namespace System;
using namespace System::Windows::Forms;


[STAThreadAttribute]
void Main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	ProtNieruch::MainWindow form;
	Application::Run(%form);
}

