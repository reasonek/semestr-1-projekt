#pragma once
#include "NieruchomoscWindow.h"
#include <fstream>
#include <chartdir.h>

namespace ProtNieruch {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Collections::Generic;
	using namespace System::Text::RegularExpressions;

	/// <summary>
	/// Podsumowanie informacji o MainWindow
	/// </summary>
	public ref class MainWindow : public System::Windows::Forms::Form
	{
	public:
		MainWindow(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~MainWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	
	private: System::Windows::Forms::Label^  AutorOpracowania;
	private: System::Windows::Forms::TextBox^  NazwiskoIImie_txt;
	private: System::Windows::Forms::Label^  NazwiskoIImie;
	private: System::Windows::Forms::Label^  Telefon;
	private: System::Windows::Forms::TextBox^  Telefon_txt;
	private: System::Windows::Forms::Label^  DataSporzadzenia;
	private: System::Windows::Forms::TextBox^  DataSporzadzenia_txt;
	private: System::Windows::Forms::Label^  DataOgledzin;
	private: System::Windows::Forms::TextBox^  DataOgledzin_txt;


	private: System::Windows::Forms::Label^  InformacjePodstawoweTitle;
	private: System::Windows::Forms::Label^  Powiat;
	private: System::Windows::Forms::Label^  Gmina;
	private: System::Windows::Forms::Label^  Miejscowosc;
	private: System::Windows::Forms::Label^  Dzielnica;
	private: System::Windows::Forms::Label^  Ulica;
	private: System::Windows::Forms::Label^  NumerBudynku;
	private: System::Windows::Forms::Label^  NumerLokalu;
	private: System::Windows::Forms::Label^  Wojewodztwo;
	private: System::Windows::Forms::TextBox^  Powiat_txt;
	private: System::Windows::Forms::TextBox^  Gmina_txt;
	private: System::Windows::Forms::TextBox^  Miejscowosc_txt;
	private: System::Windows::Forms::TextBox^  Dzielnica_txt;
	private: System::Windows::Forms::TextBox^  Ulica_txt;
	private: System::Windows::Forms::TextBox^  NumerBudynku_txt;
	private: System::Windows::Forms::TextBox^  NumerLokalu_txt;
	private: System::Windows::Forms::TextBox^  Wojewodztwo_txt;
	private: System::Windows::Forms::Label^  ParametryLokalu;
	private: System::Windows::Forms::Label^  PowierzchniaUzytkowa;
	private: System::Windows::Forms::Label^  Pietro;
	private: System::Windows::Forms::TextBox^  PowierzchniaUzytkowa_txt;
	private: System::Windows::Forms::TextBox^  Pietro_txt;
	private: System::Windows::Forms::TextBox^  OpisPrawnyITechniczny_txt;
	private: System::Windows::Forms::TextBox^  KragNabywcow_txt;
	private: System::Windows::Forms::TextBox^  OcenaJakosciNieruchomosci_txt;
	private: System::Windows::Forms::Label^  OpisPrawnyITechniczny;
	private: System::Windows::Forms::Label^  KragNabywcow;
	private: System::Windows::Forms::Label^  OcenaJakosciNieruchomosci;
	private: System::Windows::Forms::Label^  AnalizaWartosci;
	private: System::Windows::Forms::Button^  ButtonNieruchomosc;


	public: System::Windows::Forms::ListView^  ListTransakcje;
	private: System::Windows::Forms::ColumnHeader^  Lp;

	private: System::Windows::Forms::ColumnHeader^  AdresColumn;
	private: System::Windows::Forms::Button^  Zapisz;
	private: System::Windows::Forms::Button^  StworzPDF;
	private: System::Windows::Forms::Button^  ResetForm;
	private: System::Windows::Forms::ErrorProvider^  eProvider;
	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::ColumnHeader^  CenaCalkowitaColumn;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private: System::Windows::Forms::PictureBox^  pictureBox1;


	protected:

	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Metoda wymagana do obs�ugi projektanta � nie nale�y modyfikowa�
		/// jej zawarto�ci w edytorze kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->AutorOpracowania = (gcnew System::Windows::Forms::Label());
			this->NazwiskoIImie_txt = (gcnew System::Windows::Forms::TextBox());
			this->NazwiskoIImie = (gcnew System::Windows::Forms::Label());
			this->Telefon = (gcnew System::Windows::Forms::Label());
			this->Telefon_txt = (gcnew System::Windows::Forms::TextBox());
			this->DataSporzadzenia = (gcnew System::Windows::Forms::Label());
			this->DataSporzadzenia_txt = (gcnew System::Windows::Forms::TextBox());
			this->DataOgledzin = (gcnew System::Windows::Forms::Label());
			this->DataOgledzin_txt = (gcnew System::Windows::Forms::TextBox());
			this->InformacjePodstawoweTitle = (gcnew System::Windows::Forms::Label());
			this->Powiat = (gcnew System::Windows::Forms::Label());
			this->Gmina = (gcnew System::Windows::Forms::Label());
			this->Miejscowosc = (gcnew System::Windows::Forms::Label());
			this->Dzielnica = (gcnew System::Windows::Forms::Label());
			this->Ulica = (gcnew System::Windows::Forms::Label());
			this->NumerBudynku = (gcnew System::Windows::Forms::Label());
			this->NumerLokalu = (gcnew System::Windows::Forms::Label());
			this->Wojewodztwo = (gcnew System::Windows::Forms::Label());
			this->Powiat_txt = (gcnew System::Windows::Forms::TextBox());
			this->Gmina_txt = (gcnew System::Windows::Forms::TextBox());
			this->Miejscowosc_txt = (gcnew System::Windows::Forms::TextBox());
			this->Dzielnica_txt = (gcnew System::Windows::Forms::TextBox());
			this->Ulica_txt = (gcnew System::Windows::Forms::TextBox());
			this->NumerBudynku_txt = (gcnew System::Windows::Forms::TextBox());
			this->NumerLokalu_txt = (gcnew System::Windows::Forms::TextBox());
			this->Wojewodztwo_txt = (gcnew System::Windows::Forms::TextBox());
			this->ParametryLokalu = (gcnew System::Windows::Forms::Label());
			this->PowierzchniaUzytkowa = (gcnew System::Windows::Forms::Label());
			this->Pietro = (gcnew System::Windows::Forms::Label());
			this->PowierzchniaUzytkowa_txt = (gcnew System::Windows::Forms::TextBox());
			this->Pietro_txt = (gcnew System::Windows::Forms::TextBox());
			this->OpisPrawnyITechniczny_txt = (gcnew System::Windows::Forms::TextBox());
			this->KragNabywcow_txt = (gcnew System::Windows::Forms::TextBox());
			this->OcenaJakosciNieruchomosci_txt = (gcnew System::Windows::Forms::TextBox());
			this->OpisPrawnyITechniczny = (gcnew System::Windows::Forms::Label());
			this->KragNabywcow = (gcnew System::Windows::Forms::Label());
			this->OcenaJakosciNieruchomosci = (gcnew System::Windows::Forms::Label());
			this->AnalizaWartosci = (gcnew System::Windows::Forms::Label());
			this->ButtonNieruchomosc = (gcnew System::Windows::Forms::Button());
			this->ListTransakcje = (gcnew System::Windows::Forms::ListView());
			this->Lp = (gcnew System::Windows::Forms::ColumnHeader());
			this->AdresColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->CenaCalkowitaColumn = (gcnew System::Windows::Forms::ColumnHeader());
			this->Zapisz = (gcnew System::Windows::Forms::Button());
			this->StworzPDF = (gcnew System::Windows::Forms::Button());
			this->ResetForm = (gcnew System::Windows::Forms::Button());
			this->eProvider = (gcnew System::Windows::Forms::ErrorProvider(this->components));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->eProvider))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// AutorOpracowania
			// 
			this->AutorOpracowania->AutoSize = true;
			this->AutorOpracowania->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->AutorOpracowania->Location = System::Drawing::Point(12, 9);
			this->AutorOpracowania->Name = L"AutorOpracowania";
			this->AutorOpracowania->Size = System::Drawing::Size(142, 15);
			this->AutorOpracowania->TabIndex = 0;
			this->AutorOpracowania->Text = L"1. Autor opracowania";
			// 
			// NazwiskoIImie_txt
			// 
			this->NazwiskoIImie_txt->Location = System::Drawing::Point(241, 39);
			this->NazwiskoIImie_txt->Name = L"NazwiskoIImie_txt";
			this->NazwiskoIImie_txt->Size = System::Drawing::Size(173, 20);
			this->NazwiskoIImie_txt->TabIndex = 1;
			// 
			// NazwiskoIImie
			// 
			this->NazwiskoIImie->AutoSize = true;
			this->NazwiskoIImie->Location = System::Drawing::Point(12, 42);
			this->NazwiskoIImie->Name = L"NazwiskoIImie";
			this->NazwiskoIImie->Size = System::Drawing::Size(79, 13);
			this->NazwiskoIImie->TabIndex = 2;
			this->NazwiskoIImie->Text = L"Nazwisko i imi�";
			// 
			// Telefon
			// 
			this->Telefon->AutoSize = true;
			this->Telefon->Location = System::Drawing::Point(12, 67);
			this->Telefon->Name = L"Telefon";
			this->Telefon->Size = System::Drawing::Size(43, 13);
			this->Telefon->TabIndex = 3;
			this->Telefon->Text = L"Telefon";
			// 
			// Telefon_txt
			// 
			this->Telefon_txt->Location = System::Drawing::Point(241, 65);
			this->Telefon_txt->Name = L"Telefon_txt";
			this->Telefon_txt->Size = System::Drawing::Size(173, 20);
			this->Telefon_txt->TabIndex = 4;
			// 
			// DataSporzadzenia
			// 
			this->DataSporzadzenia->AutoSize = true;
			this->DataSporzadzenia->Location = System::Drawing::Point(12, 93);
			this->DataSporzadzenia->Name = L"DataSporzadzenia";
			this->DataSporzadzenia->Size = System::Drawing::Size(95, 13);
			this->DataSporzadzenia->TabIndex = 5;
			this->DataSporzadzenia->Text = L"Data sporz�dzenia";
			// 
			// DataSporzadzenia_txt
			// 
			this->DataSporzadzenia_txt->Location = System::Drawing::Point(241, 91);
			this->DataSporzadzenia_txt->Name = L"DataSporzadzenia_txt";
			this->DataSporzadzenia_txt->Size = System::Drawing::Size(173, 20);
			this->DataSporzadzenia_txt->TabIndex = 6;
			// 
			// DataOgledzin
			// 
			this->DataOgledzin->AutoSize = true;
			this->DataOgledzin->Location = System::Drawing::Point(12, 119);
			this->DataOgledzin->Name = L"DataOgledzin";
			this->DataOgledzin->Size = System::Drawing::Size(72, 13);
			this->DataOgledzin->TabIndex = 7;
			this->DataOgledzin->Text = L"Data ogl�dzin";
			// 
			// DataOgledzin_txt
			// 
			this->DataOgledzin_txt->Location = System::Drawing::Point(241, 117);
			this->DataOgledzin_txt->Name = L"DataOgledzin_txt";
			this->DataOgledzin_txt->Size = System::Drawing::Size(173, 20);
			this->DataOgledzin_txt->TabIndex = 8;
			// 
			// InformacjePodstawoweTitle
			// 
			this->InformacjePodstawoweTitle->AutoSize = true;
			this->InformacjePodstawoweTitle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->InformacjePodstawoweTitle->Location = System::Drawing::Point(12, 181);
			this->InformacjePodstawoweTitle->Name = L"InformacjePodstawoweTitle";
			this->InformacjePodstawoweTitle->Size = System::Drawing::Size(174, 15);
			this->InformacjePodstawoweTitle->TabIndex = 11;
			this->InformacjePodstawoweTitle->Text = L"2. Informacje podstawowe";
			// 
			// Powiat
			// 
			this->Powiat->AutoSize = true;
			this->Powiat->Location = System::Drawing::Point(12, 234);
			this->Powiat->Name = L"Powiat";
			this->Powiat->Size = System::Drawing::Size(39, 13);
			this->Powiat->TabIndex = 12;
			this->Powiat->Text = L"Powiat";
			// 
			// Gmina
			// 
			this->Gmina->AutoSize = true;
			this->Gmina->Location = System::Drawing::Point(12, 260);
			this->Gmina->Name = L"Gmina";
			this->Gmina->Size = System::Drawing::Size(37, 13);
			this->Gmina->TabIndex = 13;
			this->Gmina->Text = L"Gmina";
			// 
			// Miejscowosc
			// 
			this->Miejscowosc->AutoSize = true;
			this->Miejscowosc->Location = System::Drawing::Point(12, 286);
			this->Miejscowosc->Name = L"Miejscowosc";
			this->Miejscowosc->Size = System::Drawing::Size(68, 13);
			this->Miejscowosc->TabIndex = 14;
			this->Miejscowosc->Text = L"Miejscowosc";
			// 
			// Dzielnica
			// 
			this->Dzielnica->AutoSize = true;
			this->Dzielnica->Location = System::Drawing::Point(12, 312);
			this->Dzielnica->Name = L"Dzielnica";
			this->Dzielnica->Size = System::Drawing::Size(50, 13);
			this->Dzielnica->TabIndex = 15;
			this->Dzielnica->Text = L"Dzielnica";
			// 
			// Ulica
			// 
			this->Ulica->AutoSize = true;
			this->Ulica->Location = System::Drawing::Point(12, 338);
			this->Ulica->Name = L"Ulica";
			this->Ulica->Size = System::Drawing::Size(31, 13);
			this->Ulica->TabIndex = 16;
			this->Ulica->Text = L"Ulica";
			// 
			// NumerBudynku
			// 
			this->NumerBudynku->AutoSize = true;
			this->NumerBudynku->Location = System::Drawing::Point(12, 364);
			this->NumerBudynku->Name = L"NumerBudynku";
			this->NumerBudynku->Size = System::Drawing::Size(80, 13);
			this->NumerBudynku->TabIndex = 17;
			this->NumerBudynku->Text = L"NumerBudynku";
			// 
			// NumerLokalu
			// 
			this->NumerLokalu->AutoSize = true;
			this->NumerLokalu->Location = System::Drawing::Point(12, 390);
			this->NumerLokalu->Name = L"NumerLokalu";
			this->NumerLokalu->Size = System::Drawing::Size(70, 13);
			this->NumerLokalu->TabIndex = 18;
			this->NumerLokalu->Text = L"NumerLokalu";
			// 
			// Wojewodztwo
			// 
			this->Wojewodztwo->AutoSize = true;
			this->Wojewodztwo->Location = System::Drawing::Point(12, 208);
			this->Wojewodztwo->Name = L"Wojewodztwo";
			this->Wojewodztwo->Size = System::Drawing::Size(74, 13);
			this->Wojewodztwo->TabIndex = 19;
			this->Wojewodztwo->Text = L"Wojewodztwo";
			// 
			// Powiat_txt
			// 
			this->Powiat_txt->Location = System::Drawing::Point(241, 231);
			this->Powiat_txt->Name = L"Powiat_txt";
			this->Powiat_txt->Size = System::Drawing::Size(173, 20);
			this->Powiat_txt->TabIndex = 20;
			// 
			// Gmina_txt
			// 
			this->Gmina_txt->Location = System::Drawing::Point(241, 257);
			this->Gmina_txt->Name = L"Gmina_txt";
			this->Gmina_txt->Size = System::Drawing::Size(173, 20);
			this->Gmina_txt->TabIndex = 21;
			// 
			// Miejscowosc_txt
			// 
			this->Miejscowosc_txt->Location = System::Drawing::Point(241, 283);
			this->Miejscowosc_txt->Name = L"Miejscowosc_txt";
			this->Miejscowosc_txt->Size = System::Drawing::Size(173, 20);
			this->Miejscowosc_txt->TabIndex = 22;
			// 
			// Dzielnica_txt
			// 
			this->Dzielnica_txt->Location = System::Drawing::Point(241, 309);
			this->Dzielnica_txt->Name = L"Dzielnica_txt";
			this->Dzielnica_txt->Size = System::Drawing::Size(173, 20);
			this->Dzielnica_txt->TabIndex = 23;
			// 
			// Ulica_txt
			// 
			this->Ulica_txt->Location = System::Drawing::Point(241, 335);
			this->Ulica_txt->Name = L"Ulica_txt";
			this->Ulica_txt->Size = System::Drawing::Size(173, 20);
			this->Ulica_txt->TabIndex = 24;
			// 
			// NumerBudynku_txt
			// 
			this->NumerBudynku_txt->Location = System::Drawing::Point(241, 361);
			this->NumerBudynku_txt->Name = L"NumerBudynku_txt";
			this->NumerBudynku_txt->Size = System::Drawing::Size(173, 20);
			this->NumerBudynku_txt->TabIndex = 25;
			// 
			// NumerLokalu_txt
			// 
			this->NumerLokalu_txt->Location = System::Drawing::Point(241, 387);
			this->NumerLokalu_txt->Name = L"NumerLokalu_txt";
			this->NumerLokalu_txt->Size = System::Drawing::Size(173, 20);
			this->NumerLokalu_txt->TabIndex = 26;
			// 
			// Wojewodztwo_txt
			// 
			this->Wojewodztwo_txt->Location = System::Drawing::Point(241, 205);
			this->Wojewodztwo_txt->Name = L"Wojewodztwo_txt";
			this->Wojewodztwo_txt->Size = System::Drawing::Size(173, 20);
			this->Wojewodztwo_txt->TabIndex = 27;
			// 
			// ParametryLokalu
			// 
			this->ParametryLokalu->AutoSize = true;
			this->ParametryLokalu->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->ParametryLokalu->Location = System::Drawing::Point(12, 423);
			this->ParametryLokalu->Name = L"ParametryLokalu";
			this->ParametryLokalu->Size = System::Drawing::Size(131, 15);
			this->ParametryLokalu->TabIndex = 28;
			this->ParametryLokalu->Text = L"3. Parametry lokalu";
			// 
			// PowierzchniaUzytkowa
			// 
			this->PowierzchniaUzytkowa->AutoSize = true;
			this->PowierzchniaUzytkowa->Location = System::Drawing::Point(12, 452);
			this->PowierzchniaUzytkowa->Name = L"PowierzchniaUzytkowa";
			this->PowierzchniaUzytkowa->Size = System::Drawing::Size(141, 13);
			this->PowierzchniaUzytkowa->TabIndex = 29;
			this->PowierzchniaUzytkowa->Text = L"Powierzchnia u�ytkowa (m2)";
			// 
			// Pietro
			// 
			this->Pietro->AutoSize = true;
			this->Pietro->Location = System::Drawing::Point(12, 478);
			this->Pietro->Name = L"Pietro";
			this->Pietro->Size = System::Drawing::Size(121, 13);
			this->Pietro->TabIndex = 30;
			this->Pietro->Text = L"Po�o�enie lokalu (pi�tro)";
			// 
			// PowierzchniaUzytkowa_txt
			// 
			this->PowierzchniaUzytkowa_txt->Location = System::Drawing::Point(241, 449);
			this->PowierzchniaUzytkowa_txt->Name = L"PowierzchniaUzytkowa_txt";
			this->PowierzchniaUzytkowa_txt->Size = System::Drawing::Size(173, 20);
			this->PowierzchniaUzytkowa_txt->TabIndex = 31;
			// 
			// Pietro_txt
			// 
			this->Pietro_txt->Location = System::Drawing::Point(241, 475);
			this->Pietro_txt->Name = L"Pietro_txt";
			this->Pietro_txt->Size = System::Drawing::Size(173, 20);
			this->Pietro_txt->TabIndex = 32;
			// 
			// OpisPrawnyITechniczny_txt
			// 
			this->OpisPrawnyITechniczny_txt->Location = System::Drawing::Point(415, 511);
			this->OpisPrawnyITechniczny_txt->Multiline = true;
			this->OpisPrawnyITechniczny_txt->Name = L"OpisPrawnyITechniczny_txt";
			this->OpisPrawnyITechniczny_txt->Size = System::Drawing::Size(457, 51);
			this->OpisPrawnyITechniczny_txt->TabIndex = 33;
			// 
			// KragNabywcow_txt
			// 
			this->KragNabywcow_txt->Location = System::Drawing::Point(415, 568);
			this->KragNabywcow_txt->Multiline = true;
			this->KragNabywcow_txt->Name = L"KragNabywcow_txt";
			this->KragNabywcow_txt->Size = System::Drawing::Size(457, 51);
			this->KragNabywcow_txt->TabIndex = 34;
			// 
			// OcenaJakosciNieruchomosci_txt
			// 
			this->OcenaJakosciNieruchomosci_txt->Location = System::Drawing::Point(415, 625);
			this->OcenaJakosciNieruchomosci_txt->Multiline = true;
			this->OcenaJakosciNieruchomosci_txt->Name = L"OcenaJakosciNieruchomosci_txt";
			this->OcenaJakosciNieruchomosci_txt->Size = System::Drawing::Size(457, 51);
			this->OcenaJakosciNieruchomosci_txt->TabIndex = 35;
			// 
			// OpisPrawnyITechniczny
			// 
			this->OpisPrawnyITechniczny->AutoSize = true;
			this->OpisPrawnyITechniczny->Location = System::Drawing::Point(12, 525);
			this->OpisPrawnyITechniczny->Name = L"OpisPrawnyITechniczny";
			this->OpisPrawnyITechniczny->Size = System::Drawing::Size(332, 26);
			this->OpisPrawnyITechniczny->TabIndex = 36;
			this->OpisPrawnyITechniczny->Text = L"Prawna i techniczno funkcjonalna charakterystyka lokalu, \r\n w tym wyko�czenie i w"
				L"yposa�enie (opis poszczeg�lnych element�w)";
			// 
			// KragNabywcow
			// 
			this->KragNabywcow->AutoSize = true;
			this->KragNabywcow->Location = System::Drawing::Point(12, 591);
			this->KragNabywcow->Name = L"KragNabywcow";
			this->KragNabywcow->Size = System::Drawing::Size(215, 13);
			this->KragNabywcow->TabIndex = 37;
			this->KragNabywcow->Text = L"Potencjalny kr�g nabywc�w i u�ytkownik�w";
			// 
			// OcenaJakosciNieruchomosci
			// 
			this->OcenaJakosciNieruchomosci->AutoSize = true;
			this->OcenaJakosciNieruchomosci->Location = System::Drawing::Point(12, 644);
			this->OcenaJakosciNieruchomosci->Name = L"OcenaJakosciNieruchomosci";
			this->OcenaJakosciNieruchomosci->Size = System::Drawing::Size(146, 13);
			this->OcenaJakosciNieruchomosci->TabIndex = 38;
			this->OcenaJakosciNieruchomosci->Text = L"Ocena jako�ci nieruchomo�ci";
			// 
			// AnalizaWartosci
			// 
			this->AnalizaWartosci->AutoSize = true;
			this->AnalizaWartosci->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->AnalizaWartosci->Location = System::Drawing::Point(456, 9);
			this->AnalizaWartosci->Name = L"AnalizaWartosci";
			this->AnalizaWartosci->Size = System::Drawing::Size(390, 15);
			this->AnalizaWartosci->TabIndex = 39;
			this->AnalizaWartosci->Text = L"4. Analiza d�ugookresowej warto�ci rynkowej nieruchomo�ci";
			// 
			// ButtonNieruchomosc
			// 
			this->ButtonNieruchomosc->Location = System::Drawing::Point(459, 38);
			this->ButtonNieruchomosc->Name = L"ButtonNieruchomosc";
			this->ButtonNieruchomosc->Size = System::Drawing::Size(200, 23);
			this->ButtonNieruchomosc->TabIndex = 40;
			this->ButtonNieruchomosc->Text = L"Dodaj nieruchomo��";
			this->ButtonNieruchomosc->UseVisualStyleBackColor = true;
			this->ButtonNieruchomosc->Click += gcnew System::EventHandler(this, &MainWindow::ButtonNieruchomosc_Click);
			// 
			// ListTransakcje
			// 
			this->ListTransakcje->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
				this->Lp, this->AdresColumn,
					this->CenaCalkowitaColumn
			});
			this->ListTransakcje->Location = System::Drawing::Point(459, 67);
			this->ListTransakcje->Name = L"ListTransakcje";
			this->ListTransakcje->Size = System::Drawing::Size(413, 97);
			this->ListTransakcje->TabIndex = 42;
			this->ListTransakcje->UseCompatibleStateImageBehavior = false;
			this->ListTransakcje->View = System::Windows::Forms::View::Details;
			this->ListTransakcje->Items->AddRange(CreateList());
			// 
			// Lp
			// 
			this->Lp->Tag = L"";
			this->Lp->Text = L"#";
			// 
			// AdresColumn
			// 
			this->AdresColumn->Text = L"Adres";
			this->AdresColumn->Width = 248;
			// 
			// CenaCalkowitaColumn
			// 
			this->CenaCalkowitaColumn->Text = L"Cena ca�kowita";
			this->CenaCalkowitaColumn->Width = 96;
			// 
			// Zapisz
			// 
			this->Zapisz->Location = System::Drawing::Point(171, 686);
			this->Zapisz->Name = L"Zapisz";
			this->Zapisz->Size = System::Drawing::Size(173, 23);
			this->Zapisz->TabIndex = 43;
			this->Zapisz->Text = L"Zapisz";
			this->Zapisz->UseVisualStyleBackColor = true;
			this->Zapisz->Click += gcnew System::EventHandler(this, &MainWindow::Zapisz_Click);
			// 
			// StworzPDF
			// 
			this->StworzPDF->Location = System::Drawing::Point(350, 686);
			this->StworzPDF->Name = L"StworzPDF";
			this->StworzPDF->Size = System::Drawing::Size(173, 23);
			this->StworzPDF->TabIndex = 44;
			this->StworzPDF->Text = L"Stw�rz PDF";
			this->StworzPDF->UseVisualStyleBackColor = true;
			this->StworzPDF->Click += gcnew System::EventHandler(this, &MainWindow::StworzPDF_Click);
			// 
			// ResetForm
			// 
			this->ResetForm->Location = System::Drawing::Point(529, 686);
			this->ResetForm->Name = L"ResetForm";
			this->ResetForm->Size = System::Drawing::Size(173, 23);
			this->ResetForm->TabIndex = 45;
			this->ResetForm->Text = L"Reset";
			this->ResetForm->UseVisualStyleBackColor = true;
			this->ResetForm->Click += gcnew System::EventHandler(this, &MainWindow::ResetForm_Click);
			// 
			// eProvider
			// 
			this->eProvider->ContainerControl = this;
			// 
			// pictureBox1
			// 
			this->pictureBox1->ImageLocation = Path::GetFullPath("barlabel.png");
			this->pictureBox1->Location = System::Drawing::Point(459, 187);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(413, 308);
			this->pictureBox1->TabIndex = 46;
			this->pictureBox1->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(748, 37);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(124, 23);
			this->button1->TabIndex = 47;
			this->button1->Text = L"Od�wie� list�";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainWindow::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(665, 37);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 48;
			this->button2->Text = L"Usu�";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainWindow::button2_Click);
			// 
			// MainWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(902, 721);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->ResetForm);
			this->Controls->Add(this->StworzPDF);
			this->Controls->Add(this->Zapisz);
			this->Controls->Add(this->ListTransakcje);
			this->Controls->Add(this->ButtonNieruchomosc);
			this->Controls->Add(this->AnalizaWartosci);
			this->Controls->Add(this->OcenaJakosciNieruchomosci);
			this->Controls->Add(this->KragNabywcow);
			this->Controls->Add(this->OpisPrawnyITechniczny);
			this->Controls->Add(this->OcenaJakosciNieruchomosci_txt);
			this->Controls->Add(this->KragNabywcow_txt);
			this->Controls->Add(this->OpisPrawnyITechniczny_txt);
			this->Controls->Add(this->Pietro_txt);
			this->Controls->Add(this->PowierzchniaUzytkowa_txt);
			this->Controls->Add(this->Pietro);
			this->Controls->Add(this->PowierzchniaUzytkowa);
			this->Controls->Add(this->ParametryLokalu);
			this->Controls->Add(this->Wojewodztwo_txt);
			this->Controls->Add(this->NumerLokalu_txt);
			this->Controls->Add(this->NumerBudynku_txt);
			this->Controls->Add(this->Ulica_txt);
			this->Controls->Add(this->Dzielnica_txt);
			this->Controls->Add(this->Miejscowosc_txt);
			this->Controls->Add(this->Gmina_txt);
			this->Controls->Add(this->Powiat_txt);
			this->Controls->Add(this->Wojewodztwo);
			this->Controls->Add(this->NumerLokalu);
			this->Controls->Add(this->NumerBudynku);
			this->Controls->Add(this->Ulica);
			this->Controls->Add(this->Dzielnica);
			this->Controls->Add(this->Miejscowosc);
			this->Controls->Add(this->Gmina);
			this->Controls->Add(this->Powiat);
			this->Controls->Add(this->InformacjePodstawoweTitle);
			this->Controls->Add(this->DataOgledzin_txt);
			this->Controls->Add(this->DataOgledzin);
			this->Controls->Add(this->DataSporzadzenia_txt);
			this->Controls->Add(this->DataSporzadzenia);
			this->Controls->Add(this->Telefon_txt);
			this->Controls->Add(this->Telefon);
			this->Controls->Add(this->NazwiskoIImie);
			this->Controls->Add(this->NazwiskoIImie_txt);
			this->Controls->Add(this->AutorOpracowania);
			this->Name = L"MainWindow";
			this->Text = L"Protok� Nieruchomo�ci";
			this->Load += gcnew System::EventHandler(this, &MainWindow::MainWindow_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->eProvider))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void MainWindow_Load(System::Object^  sender, System::EventArgs^  e) {

	CreatingChart();
	array<String^>^ directory = Directory::GetFileSystemEntries("Ekspertyza");
	if (directory->Length > 0) {
		LoadEkspertyza();
	}
}
private: Void CreatingChart() {
	String^ folder = "Nieruchomosc";

	array<String^>^Transactions = GetTransactions(folder);

	if (Transactions->Length > 0) {
		List<double>^ Prices = gcnew List<double>();
		for (int i = 0; Transactions->Length > i; i++) {
			String^ filepath = Transactions[i];
			double Price = Transaction(filepath);
			Prices->Add(Price);
		}

		//Making Chart

		CreateChart(Prices, Transactions);
	}
}
private: System::Void ButtonNieruchomosc_Click(System::Object^  sender, System::EventArgs^  e) {

	String^ folder = "Nieruchomosc";
	array<String^>^ directory = Directory::GetFileSystemEntries(folder);
	if (directory->Length < 8) {
		ProtNieruch1::NieruchomoscWindow Nieruchomosc;
		Nieruchomosc.FormClosing += gcnew FormClosingEventHandler(this, &MainWindow::Nieruchomosc_FormClosing);
		Nieruchomosc.ShowDialog();
	}
	else {
		MessageBox::Show("maksymalnie mo�na doda� 8 transakcji");
	}
}
public: Void Nieruchomosc_FormClosing(Object^ sender, FormClosingEventArgs^ e) {
	RefreshingList();
	CreatingChart();
	this->pictureBox1->ImageLocation = Path::GetFullPath("barlabel.png");

}
public: Void RefreshingList() {

	this->ListTransakcje->Items->Clear();
	array<ListViewItem^>^ List = CreateList();
	this->ListTransakcje->Items->AddRange(List);
}
private: System::Void ResetForm_Click(System::Object^  sender, System::EventArgs^  e) {

	NazwiskoIImie_txt->Text = "";
	Telefon_txt->Text = "";
	DataSporzadzenia_txt->Text = "";
	DataOgledzin_txt->Text = "";
	Wojewodztwo_txt->Text = "";
	Powiat_txt->Text = "";
	Gmina_txt->Text = "";
	Miejscowosc_txt->Text = "";
	Dzielnica_txt->Text = "";
	Ulica_txt->Text = "";
	NumerBudynku_txt->Text = "";
	NumerLokalu_txt->Text = "";
	PowierzchniaUzytkowa_txt->Text = "";
	Pietro_txt->Text = "";
	OpisPrawnyITechniczny_txt->Text = "";
	KragNabywcow_txt->Text = "";
	OcenaJakosciNieruchomosci_txt->Text = "";
}

private: System::Void Zapisz_Click(System::Object^  sender, System::EventArgs^  e) 
{
		String^ filename = "Ekspertyza//Ekpsertyza.csv";
		createandSaveCSV(filename);
}
private: Void LoadEkspertyza() {

	String^ filename = "Ekspertyza//Ekpsertyza.csv";
	StreamReader^ file = File::OpenText(filename);
	String^ str;
	int count = 0;
	str = file->ReadToEnd();
	array<String^>^ Controls = str->Split(',');	

	NazwiskoIImie_txt->Text = Controls[0];
	Telefon_txt->Text = Controls[1];
	DataSporzadzenia_txt->Text = Controls[2];
	DataOgledzin_txt->Text = Controls[3];
	Wojewodztwo_txt->Text = Controls[4];
	Powiat_txt->Text = Controls[5];
	Gmina_txt->Text = Controls[6];
	Miejscowosc_txt->Text = Controls[7];
	Dzielnica_txt->Text = Controls[8];
	Ulica_txt->Text = Controls[9];
	NumerBudynku_txt->Text = Controls[10];
	NumerLokalu_txt->Text = Controls[11];
	PowierzchniaUzytkowa_txt->Text = Controls[12];
	Pietro_txt->Text = Controls[13];
	OpisPrawnyITechniczny_txt->Text = Controls[14];
	KragNabywcow_txt->Text = Controls[15];
	OcenaJakosciNieruchomosci_txt->Text = Controls[16];
	file->Close();
}
private: System::Void StworzPDF_Click(System::Object^  sender, System::EventArgs^  e) {

}

private: double Transaction(String^ filepath) {
	StreamReader^ file = File::OpenText(filepath);
	String^ str;
	int count = 0;
	str = file->ReadToEnd();
	
	array<String^>^ columns = str->Split(',');
	double Cena = System::Convert::ToDouble(columns[2]);
	file->Close();
	return Cena;
}
private: array<String^>^ GetTransactions(String^ DirectoryName) {
	array<String^>^ directory = Directory::GetFileSystemEntries(DirectoryName);

	return directory;
}
public: void CreateChart(List<double>^ Prices, array<String^>^ label)
{
	array<double>^ data = gcnew array<double>(Prices->Count);
	for (int i = 0; Prices->Count > i; i++) {
		double price = Prices[i];
		data[i]=price;
	}
	if(Prices->Count == 1)
	{ 
		double datas[] = { Prices[0] };
	}
	else 
	{
		double datas[] = { Prices[0] };
	}

	// Create XYChart
	XYChart *c = new XYChart(413, 308);

	char* labels = new char[label->Length];

	double data1[] = { 0 };
	double data2[] = { 0, 0 };
	double data3[] = { 0, 0, 0 };
	double data4[] = { 0, 0, 0, 0 };
	double data5[] = { 0, 0, 0, 0, 0 };
	double data6[] = { 0, 0, 0, 0, 0, 0 };
	double data7[] = { 0, 0, 0, 0, 0, 0, 0 };
	double data8[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	double data9[] = { 0 };

	const char *labels1[] = { "" };
	const char *labels2[] = { "", "" };
	const char *labels3[] = { "", "", "" };
	const char *labels4[] = { "", "", "", "" };
	const char *labels5[] = { "", "", "", "", "" };
	const char *labels6[] = { "", "", "", "", "", "" };
	const char *labels7[] = { "", "", "", "", "", "", "" };
	const char *labels8[] = { "", "", "", "", "", "", "", "" };

	switch (label->Length) {
	case 1:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels1, (int)(sizeof(labels1) / sizeof(labels1[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data1[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data1, (int)(sizeof(data1) / sizeof(data1[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 2:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels2, (int)(sizeof(labels2) / sizeof(labels2[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data2[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data2, (int)(sizeof(data2) / sizeof(data2[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 3:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels3, (int)(sizeof(labels3) / sizeof(labels3[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data3[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data3, (int)(sizeof(data3) / sizeof(data3[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 4:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels4, (int)(sizeof(labels4) / sizeof(labels4[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data4[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data4, (int)(sizeof(data4) / sizeof(data4[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 5:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels5, (int)(sizeof(labels5) / sizeof(labels5[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data5[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data5, (int)(sizeof(data5) / sizeof(data5[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 6:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels6, (int)(sizeof(labels6) / sizeof(labels6[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data6[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data6, (int)(sizeof(data6) / sizeof(data6[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 7:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels7, (int)(sizeof(labels7) / sizeof(labels7[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data7[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data7, (int)(sizeof(data7) / sizeof(data7[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	case 8:
		// Set the labels on the x axis.
		c->xAxis()->setLabels(StringArray(labels8, (int)(sizeof(labels8) / sizeof(labels8[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data8[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data8, (int)(sizeof(data8) / sizeof(data8[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	default:
		c->xAxis()->setLabels(StringArray(labels1, (int)(sizeof(labels1) / sizeof(labels1[0]))));
		// Add a blue (0x6699bb) bar chart layer with transparent border using the given data
		for (int i = 0; Prices->Count > i; i++) {
			data9[i] = Prices[i];
		}
		c->addBarLayer(DoubleArray(data9, (int)(sizeof(data9) / sizeof(data9[0]))), 0x6699bb
		)->setBorderColor(Chart::Transparent);
		break;
	}

	// Ploting and GridLines
	c->setPlotArea(70, 60, 313, 208, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Set the x and y axis stems to transparent and the label font to 12pt Arial
	c->xAxis()->setColors(Chart::Transparent);
	c->yAxis()->setColors(Chart::Transparent);
	c->xAxis()->setLabelStyle("arial.ttf", 10);
	c->yAxis()->setLabelStyle("arial.ttf", 10);

	// For the automatic y-axis labels, set the minimum spacing to 40 pixels.
	c->yAxis()->setTickDensity(20);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial Bold font
	c->yAxis()->setTitle("Cena transakcji", "arialbd.ttf", 12, 0x555555);

	// Outputing chart to file
	c->makeChart("barlabel.png");

	//free up resources
	delete c;
}
private: void createandSaveCSV(System::String^ filename)
{
	int validation = Validating();

	if (validation > 0)
	{
		MessageBox::Show("W formularzu znajduj� si� b��dy");
	}
	else
	{
		// file pointer 

		StreamWriter^ fout = gcnew StreamWriter(filename);

		//Writing to Stream/file and close stream/file
			
		fout->Write(NazwiskoIImie_txt->Text + ",");
		fout->Write(Telefon_txt->Text + ",");
		fout->Write(DataSporzadzenia_txt->Text + ",");
		fout->Write(DataOgledzin_txt->Text + ",");
		fout->Write(Wojewodztwo_txt->Text + ",");
		fout->Write(Powiat_txt->Text + ",");
		fout->Write(Gmina_txt->Text + ",");
		fout->Write(Miejscowosc_txt->Text + ",");
		fout->Write(Dzielnica_txt->Text + ",");
		fout->Write(Ulica_txt->Text + ",");
		fout->Write(NumerBudynku_txt->Text + ",");
		fout->Write(NumerLokalu_txt->Text + ",");
		fout->Write(PowierzchniaUzytkowa_txt->Text + ",");
		fout->Write(Pietro_txt->Text + ",");
		fout->Write(OpisPrawnyITechniczny_txt->Text + ",");
		fout->Write(KragNabywcow_txt->Text + ",");
		fout->Write(OcenaJakosciNieruchomosci_txt->Text + ",");
		fout->Close();

		//Massage after adding file

		MessageBox::Show("Dodano transakcj� poprawnie");
	}
}
private: array<ListViewItem^>^ CreateList() {

	String^ folder = "Nieruchomosc";
	array<String^>^Transactions = GetTransactions(folder);
	int z = Transactions->Length;

	array<ListViewItem^>^ List = gcnew array<ListViewItem^>(z);

	if (Transactions->Length > 0) {
		for (int i = 0; Transactions->Length > i; i++) {
			String^ filepath = Transactions[i];
			StreamReader^ file = File::OpenText(filepath);
			String^ str;
			str = file->ReadToEnd();
			String^ Lp = Convert::ToString(i+1);
			array<String^>^ columns = str->Split(',');
			array<String^>^ column = gcnew array<String^> {columns[0], columns[1], columns[2]};			
			ListViewItem^ ListViewItem1 = gcnew ListViewItem(column);
			List[i] = ListViewItem1;
			file->Close();
		}
	}
	return List;
}
/*private: Void Nieruchomosc_FormClosing(Object^ sender, FormClosingEventArgs e)
{
	array<ListViewItem^>^ List = CreateList();
	ListTransakcje->Items->Clear();
	ListTransakcje->Items->AddRange(List);
}*/
	private: int Validating() {
		int flag = 0;

		//Null Validations

		if (NazwiskoIImie_txt->Text == "") {
			eProvider->SetError(NazwiskoIImie_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(NazwiskoIImie_txt, "");
		}

		if (Telefon_txt->Text == "") {
			eProvider->SetError(Telefon_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Telefon_txt, "");
		}

		if (DataSporzadzenia_txt->Text == "") {
			eProvider->SetError(Telefon_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Telefon_txt, "");
		}

		if (DataSporzadzenia_txt->Text == "") {
			eProvider->SetError(DataSporzadzenia_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(DataSporzadzenia_txt, "");
		}

		if (DataOgledzin_txt->Text == "") {
			eProvider->SetError(DataOgledzin_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(DataOgledzin_txt, "");
		}

		if (Wojewodztwo_txt->Text == "") {
			eProvider->SetError(Wojewodztwo_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Wojewodztwo_txt, "");
		}

		if (Powiat_txt->Text == "") {
			eProvider->SetError(Powiat_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Powiat_txt, "");
		}

		if (Gmina_txt->Text == "") {
			eProvider->SetError(Gmina_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Gmina_txt, "");
		}

		if (Miejscowosc_txt->Text == "") {
			eProvider->SetError(Miejscowosc_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Miejscowosc_txt, "");
		}

		if (Ulica_txt->Text == "") {
			eProvider->SetError(Ulica_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Ulica_txt, "");
		}

		if (NumerBudynku_txt->Text == "") {
			eProvider->SetError(NumerBudynku_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(NumerBudynku_txt, "");
		}

		if (NumerLokalu_txt->Text == "") {
			eProvider->SetError(NumerLokalu_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(NumerLokalu_txt, "");
		}

		if (PowierzchniaUzytkowa_txt->Text == "") {
			eProvider->SetError(PowierzchniaUzytkowa_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(PowierzchniaUzytkowa_txt, "");
		}

		if (Pietro_txt->Text == "") {
			eProvider->SetError(Pietro_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(Pietro_txt, "");
		}

		if (OpisPrawnyITechniczny_txt->Text == "") {
			eProvider->SetError(OpisPrawnyITechniczny_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(OpisPrawnyITechniczny_txt, "");
		}

		if (KragNabywcow_txt->Text == "") {
			eProvider->SetError(KragNabywcow_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(KragNabywcow_txt, "");
		}

		if (OcenaJakosciNieruchomosci_txt->Text == "") {
			eProvider->SetError(OcenaJakosciNieruchomosci_txt, "Pole nie mo�e by� puste");
			flag++;
		}
		else
		{
			eProvider->SetError(OcenaJakosciNieruchomosci_txt, "");
		}

		//Additional Validations
		//still more TODO:


		return flag;
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	RefreshingList();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (ListTransakcje->FocusedItem) {
		String^ Item = ListTransakcje->FocusedItem->Text;
		File::Delete("Nieruchomosc\\" + Item + ".csv");
		CreatingChart();
		this->pictureBox1->ImageLocation = Path::GetFullPath("barlabel.png");
		RefreshingList();
	}
	else {
		MessageBox::Show("Nale�y zaznaczy� rekord");
	}
}
};
}
